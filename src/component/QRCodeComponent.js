import React, { useState, useEffect } from 'react';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';

import AutoCheckInPopUp from '../container/AutoCheckinPopUp';

import Styled from '../styled/QRCodeStyle';
import { CheckInGuest, getAllGuest } from '../services/CheckInService';
import {
  playSuccessSound,
  playFailSound,
} from '../services/SoundService';

const QRScannerComponent = () => {

  const [isScanned, setIsScanned] = useState(false);
  const [visible, setVisible] = useState(false);
  const [popUpData, setPopUpData] = useState({});
  const [popUpCase, setPopUpCase] = useState('Success');
  const [totalGuest, setTotalGuest] = useState(0);
  const [checkedInGuest, setCheckedInGuest] = useState(0);
  
  const title = 'QRCode Check-In';
  // Ask Permission
  useEffect(() => {
    getPermission();
  }, []);

  useEffect(() => {
    getGuestList();
  }, []);

  async function getPermission() {
    const {status} = await Permissions.askAsync(Permissions.CAMERA);
      if (status === 'granted') {
        setIsScanned(false);
      } else {
        alert('Access camera permission to use this fuction');
      }
  };
  
  const handleBarCodeScanner = ({type, data}) => {
    if (isScanned === false) {
      handleCheckIn(data);
    }
  }

  async function handleCheckIn(data) {
    setIsScanned(true);
    const response = await CheckInGuest(data);
    switch (response.statusCode) {
      case 200:
        if (response.checkedInCount === 1) {
          setVisible(true);
          playSuccessSound();
          setPopUpCase('Success')
          setPopUpData(response)
        }
        else {
          setVisible(true);
          playFailSound();
          setPopUpCase('checkedIn');
          setPopUpData(response);
        }
        break;
      case 404:
        setVisible(true);
        playFailSound();
        setPopUpCase('noExist');
        setPopUpData(response);
        break;
    }
  };

  const closeDialog = () => {
    setPopUpData({});
    getGuestList();
    setIsScanned(false);
    setVisible(false);
  }

  async function getGuestList() {
    let count = 0;
    const guests = await getAllGuest();
    setTotalGuest(guests.length);
    guests.forEach((guest) => {
      if (guest.checkedInFlag === true) {
        count += 1;
      }
    });
    setCheckedInGuest(count);
  };

  const resetCamera = () => {
    setVisible(false);
    setIsScanned(false);
  }

  return (
    <BarCodeScanner  
    onBarCodeScanned={handleBarCodeScanner}
    style={[StyleSheet.absoluteFill, Styled.container]}
    >
      <View>
        <Text style={Styled.title}>{title}</Text>
        <Text style={Styled.countContainer}>{checkedInGuest}/{totalGuest}</Text>
      </View>
      <View style={Styled.layerTop} />
      <View style={Styled.layerCenter}>
        <View style={Styled.layerLeft} />
        <View style={Styled.focused} />
        <View style={Styled.layerRight} />
      </View>
      <View style={Styled.layerBottom} />
        <TouchableOpacity
          style={Styled.resetButton}
          onPress={resetCamera}
        >
          <Text>Reset Camera</Text>
        </TouchableOpacity>
      <Dialog
        visible={visible}
        onTouchOutside={closeDialog}
      >
        <DialogContent style={Styled.dialogContent}>
          <AutoCheckInPopUp
            closeDialog={closeDialog}
            popUpCase={popUpCase}
            popUpData={popUpData}
          />
        </DialogContent>
      </Dialog>
    </BarCodeScanner>
  );
};

export default QRScannerComponent;