import React, { useState, useEffect } from 'react';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import {
  View,
  TextInput,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  ActivityIndicator,
} from 'react-native';

import GuestInfo from '../container/GuestInfo';
import Styled from '../styled/ManualCheckInStyle';
import { getAllGuest, CheckInGuest } from '../services/CheckInService';
import {
  playSuccessSound,
} from '../services/SoundService';

const ManualCheckIn = () => {
  const title = 'Checkin Event';
  const [guestList, setGuestList] = useState([]);
  const [keyword, setKeyWord] = useState('');
  const [visible, setVisible] = useState(false);
  const [guest, setGuest] = useState({});
  const [animating, setAnimating] = useState(true);
  const [totalGuest, setTotalGuest] = useState(0);
  const [checkedInGuest, setCheckedInGuest] = useState(0);

  useEffect(() => {
    getGuestList();
  }, []);

  async function getGuestList() {
    let count = 0;
    const guests = await getAllGuest();
    setGuestList(guests);
    setTotalGuest(guests.length);
    guests.forEach((guest) => {
      if (guest.checkedInFlag === true) {
        count += 1;
      }
    });
    setCheckedInGuest(count);
    setAnimating(false);
  };

  function searchGuest() {
    const resultArray = [];
    guestList.forEach(guest => {
      if (guest.fullName.toLowerCase().includes(keyword.toLowerCase())) {
        resultArray.push(guest);
      } else if (guest.email && guest.email.toLowerCase().includes(keyword.toLowerCase())) {
        resultArray.push(guest);
      } else if (guest.activationCode.toLowerCase().includes(keyword.toLowerCase())) {
        resultArray.push(guest);
      }
    });
    return resultArray;
  }

  handleSearch = async () => {
    setAnimating(true);
    if (keyword !== '') {
      const resultArray = await searchGuest();
      console.log(resultArray);
      if (resultArray.length > 0) {
        setGuestList(resultArray);
      } else {
        alert("Guest not found!");
      }
    }
    setAnimating(false);
  };

  const resetClearData = () => {
    setKeyWord('');
    getGuestList();
  };

  const closeDialog = () => {
    setVisible(false);
  }

  async function handleCheckIn(activationCode) {
    setAnimating(true);
    const response = await CheckInGuest(activationCode);
    if (response.statusCode === 200) {
      playSuccessSound();
      getGuestList();
      setVisible(false);
    } else {
      setVisible(false); 
    }
  };

  return (
    <View>
      <Text style={Styled.countContainer}>{checkedInGuest}/{totalGuest}</Text>
      <Image
        resizeMode="contain"
        style={Styled.logo}
        source={require('../../assets/logoACB.png')}
      />
      <Text style={Styled.title}>{title}</Text>
      <TextInput
        style={Styled.searchBox}
        onChangeText={(text) => setKeyWord(text)}
        onSubmitEditing={handleSearch}
        onFocus={resetClearData}
        value={keyword}
        placeholder="Search by full name, email or activation code"
      />
      <View style={Styled.listViewContainer}>
        <ActivityIndicator
          style={Styled.loader}
          size="large"
          color="wheat"
          animating={animating}
        />
        { guestList.length !== 0 ? (
          <FlatList
            data={guestList}
            renderItem={
              ({item, index}) =>
                <GuestInfo
                  index={index + 1}
                  guestInfo={item}
                  setVisible={setVisible}
                  setGuest={setGuest}
                />
              }
          />
        ) : null}
      </View>
      <Dialog
        visible={visible}
        onTouchOutside={closeDialog}
      >
        <DialogContent style={Styled.dialogContent}>
          <Text style={[Styled.myText, Styled.dialogTitle]}>Check-in Comfirm</Text>
          <View style={Styled.guestInfoContainer}>
            <Text style={Styled.myText}>Guest name: {guest.fullName}</Text>
            <Text style={Styled.myText}>Email: {guest.email}</Text>
            <Text style={Styled.myText}>Activation code: {guest.activationCode}</Text>
          </View>
          <View style={Styled.dialogButtonContainer}>
            <TouchableOpacity
              onPress={() => handleCheckIn(guest.activationCode)}
              style={Styled.myButtonCheckin}
            >
              <Text style={Styled.myBtnText}>Check-in</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={closeDialog}
              style={Styled.myButtonCancel}
            >
              <Text style={Styled.myBtnText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </DialogContent>
      </Dialog>
    </View>
  )
};

export default ManualCheckIn;