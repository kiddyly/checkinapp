import { StyleSheet } from 'react-native';
import {
        widthPercentageToDP as wp,
        heightPercentageToDP as hp
        } from 'react-native-responsive-screen';

export default StyleSheet.create({
  loader: {
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 2,
  },
  logo: {
    position: 'absolute',
    top: -hp('45%'),
    alignSelf: 'center',
    width: wp('25%'),
    height: hp('20%'),
  },
  searchBox: {
    height: 35,
    borderRadius: 9,
    borderColor: 'white',
    borderWidth: 1,
    alignSelf: 'center',
    paddingLeft: 5,
    top: -hp('30%'),
    width: wp('90%'),
    backgroundColor: 'white',
  },
  title: {
    top: -hp('32%'),
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'monospace',
    fontSize: 24,
  },
  listViewContainer: {
    position: 'absolute',
    top: -hp('18%'),
    justifyContent: 'center',
    alignSelf: 'center',
    height: hp('70%'),
  },
  dialogContent: {
    paddingTop: 10,
    justifyContent: 'center',
    height: hp('35%'),
    width: wp('80%'),
  },
  dialogButtonContainer: {
    top: hp('5%'),
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderTopWidth: 0.5,
    width: wp('60%'),
    paddingTop: 10,
    alignSelf: 'center',
  },
  myButtonCheckin: {
    backgroundColor: "#03087C",
    height: hp('5%'),
    width: wp('25%'),
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  myButtonCancel: {
    backgroundColor: "#9A2320",
    height: hp('5%'),
    width: wp('25%'),
    borderRadius: 13,
    justifyContent: 'center',
    alignItems: 'center',
  },
  dialogTitle: {
    alignSelf: 'center',
    fontSize: 18,
    bottom: hp('3%'),
    width: wp('60%'),
    textAlign: 'center',
    paddingBottom: 7,
    borderBottomWidth: 0.5,
  },
  guestInfoContainer: {
    alignSelf: 'flex-start',
    justifyContent: 'space-between',
    height: hp('10%'),
    paddingLeft: wp('5%'),
  },
  myText: {
    fontWeight: 'bold',
    fontFamily: 'monospace',
  },
  myBtnText: {
    color: 'white',
    fontWeight: 'bold',
  },
  countContainer: {
    backgroundColor: 'white',
    position: 'absolute',
    top: -hp('31%'),
    right: wp('4%'),
    width: wp('15%'),
    height: hp('3%'),
    textAlign: 'center',
    borderRadius: 5,
    fontFamily: 'monospace',
    fontWeight: 'bold',
  }
})