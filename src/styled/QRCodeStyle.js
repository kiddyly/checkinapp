import { StyleSheet } from 'react-native';
import {
        widthPercentageToDP as wp,
        heightPercentageToDP as hp
        } from 'react-native-responsive-screen';

const opacity = 'rgba(0, 0, 0, .6)';
export default StyleSheet.create({
  container: {
    position: 'absolute',
    flexDirection: 'column'
  },
  layerTop: {
    flex: 1.5,
    backgroundColor: opacity
  },
  layerCenter: {
    flex: 1.5,
    flexDirection: 'row'
  },
  layerLeft: {
    flex: 1,
    backgroundColor: opacity
  },
  focused: {
    flex: 10
  },
  layerRight: {
    flex: 1,
    backgroundColor: opacity
  },
  layerBottom: {
    flex: 1.5,
    backgroundColor: opacity
  },
  title: {
    position: 'absolute',
    top: hp('10%'),
    alignSelf: 'center',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 24,
  },
  countContainer: {
    backgroundColor: 'white',
    position: 'absolute',
    top: hp('18%'),
    right: wp('4%'),
    width: wp('15%'),
    height: hp('3%'),
    textAlign: 'center',
    borderRadius: 5,
    fontFamily: 'monospace',
    fontWeight: 'bold',
  },
  resetButton: {
    backgroundColor: 'white',
    position: 'absolute',
    top: hp('18%'),
    right: wp('25%'),
    width: wp('25%'),
    height: hp('3%'),
    borderRadius: 5,
    fontFamily: 'monospace',
    fontWeight: 'bold',
    alignItems: 'center',
    zIndex: 1000,
  }
})