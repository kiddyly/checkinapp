import { StyleSheet } from 'react-native';
import Constants from 'expo-constants';
import {
        widthPercentageToDP as wp,
        heightPercentageToDP as hp
        } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Constants.statusBarHeight,
  },
  myButton: {
    position: 'absolute',
    top: hp('10%'),
    right: wp('12%'),
    alignSelf: 'center',
    backgroundColor: 'white',
    borderRadius: 25,
    width: 35,
    height: 35,
    justifyContent: 'center',
  },
  myButtonConfig: {
    position: 'absolute',
    top: hp('10%'),
    right: wp('2%'),
    alignSelf: 'center',
    borderRadius: 25,
    width: 35,
    height: 35,
    justifyContent: 'center',
  },
  icon: {
    alignSelf: 'center',
    width: wp('6%'),
    height: wp('6%'),
  },
  dialogContent: {
    paddingTop: 10,
    justifyContent: 'center',
    height: hp('25%'),
    width: wp('80%'),
  },
  textInput: {
    justifyContent: 'center',
    paddingLeft: 5,
    borderWidth: 0.5,
    borderRadius: 10,
  }
})