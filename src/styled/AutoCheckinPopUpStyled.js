import { StyleSheet } from 'react-native';
import {
	widthPercentageToDP as wp,
	heightPercentageToDP as hp
	} from 'react-native-responsive-screen';

export default StyleSheet.create({
	container: {
		paddingTop: 13,
		justifyContent: 'space-around',
	},
	myText: {
		fontWeight: 'bold',
		alignSelf: 'center',
		fontFamily: 'monospace',
	},
	itemTextCheckin: {
		fontWeight: "bold",
    fontFamily: 'monospace',
    color: 'green',
	},
	myButtonClose: {
		backgroundColor: "#0D2076",
    height: hp('5%'),
    width: wp('25%'),
    borderRadius: 10,
		alignSelf: 'center',
		justifyContent: 'center',
    marginTop: 5,
	},
})

