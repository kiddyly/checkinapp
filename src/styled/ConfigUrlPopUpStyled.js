import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
  } from 'react-native-responsive-screen';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  title: {
    fontWeight: "bold",
    marginTop: hp('3%'),
    alignSelf: 'center',
    fontFamily: 'monospace',
    color: '#0D2076',
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: wp('60%'),
    alignSelf: 'center',
  },
  myButton: {
    borderRadius: 10,
    height: hp('5%'),
    width: wp('25%'),
    justifyContent: 'center',
  },
  textInput: {
    justifyContent: 'center',
    paddingLeft: 5,
    borderWidth: 0.5,
    borderRadius: 10,
  },
  myText: {
    color: 'white',
    fontWeight: 'bold',
    alignSelf: 'center',
  },
  myButtonReset: {
    position: 'absolute',
    right: 0,
  },
})