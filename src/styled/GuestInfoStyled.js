import { StyleSheet } from 'react-native';
import {
        widthPercentageToDP as wp,
        heightPercentageToDP as hp
        } from 'react-native-responsive-screen';

export default StyleSheet.create({
  itemContainer: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    borderRadius: 10,
    width: wp('90%'),
    height: hp('15%'),
    marginBottom: hp('1%'),
    backgroundColor: 'white',
    paddingTop: 5,
  },
  itemText: {
    fontWeight: "bold",
    fontFamily: 'monospace',
    paddingLeft: hp('3%'),
  },
  itemTextCheckin: {
    fontWeight: "bold",
    fontFamily: 'monospace',
    paddingLeft: hp('3%'),
    color: 'green',
  },
  checkInButton: {
    position: 'absolute',
    top: 8,
    right: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 10,
    backgroundColor: '#0D2076',
    borderRadius: 10,
    height: hp('5%'),
    width: wp('25%'),
  },
})