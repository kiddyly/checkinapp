import React, { useState, useEffect } from 'react';
import {
	View,
	Text,
	TouchableOpacity
} from 'react-native';

import PropTypes from 'prop-types';
import moment from 'moment';

import Styled from '../styled/GuestInfoStyled';


const GuestInfo = (props) => {
	const [guestInitial, setGuestInitial] = useState({});
	const {
		guestInfo,
		setVisible,
		setGuest,
		index,
	} = props;

	useEffect(() => {
		let checkedInTime = '';
		if (guestInfo.checkedInFlag) {
			checkedInTime = moment(guestInfo.checkedInDateTime).format('h:mm a, DD-MM-YYYY');
		}
		setGuestInitial({
			fullName: guestInfo.fullName,
			email: guestInfo.email,
			activationCode: guestInfo.activationCode,
			checkIn: checkedInTime,
		})
	})

	return (
		<View style={Styled.itemContainer}>
			<Text style={Styled.itemText}>No.{index}</Text>
			<Text style={Styled.itemText}>{guestInitial.fullName} ({guestInitial.activationCode})</Text>
			<Text style={Styled.itemText}>Email: {guestInitial.email}</Text>
			{guestInfo.checkedInFlag ? (
				<Text style={Styled.itemTextCheckin}>Check-in time: {guestInitial.checkIn}</Text>
			) : (
				<TouchableOpacity
					onPress={() => {setVisible(true); setGuest(guestInfo)}}
					style={Styled.checkInButton}
				>
					<Text style={{ fontWeight: 'bold', color: 'white' }}>Check-in</Text>
				</TouchableOpacity>
			)}
		</View>
	)
}

GuestInfo.propTypes = {
	guestInfo: PropTypes.oneOfType([PropTypes.object, PropTypes.func]).isRequired,
	setVisible: PropTypes.func.isRequired,
	setGuest: PropTypes.func.isRequired,
	index: PropTypes.number.isRequired,
}

export default GuestInfo;