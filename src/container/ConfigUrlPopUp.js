import React, { useState } from 'react';
import {
  TextInput,
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

import PropTypes from 'prop-types';
import { configUrl } from '../services/CheckInService';
import { validURL } from '../utils/index';
import Styled from '../styled/ConfigUrlPopUpStyled';

const ConfigUrlPopUp = (props) => {
  const { setVisible } = props;
  const defaultUrl = 'http://checkinevent.azurewebsites.net';
  const [url, setUrl] = useState('http://checkinevent.azurewebsites.net');

  const handleConfig = () => {
    const checkUrl = validURL(url);
    if (!checkUrl) {
      alert('Invalid url, please re-check url!');
    } else {
      const isSuccess = configUrl(url);
      if (isSuccess) {
        alert('Config successed');
        setVisible(false);
      } else {
        alert('We cannot config url now, please try again!');
      }
    }
  }

  const handleResetDefault = () => {
    setUrl(defaultUrl);
  }

  return (
    <View style={Styled.container}>
      <Text style={Styled.title}>Config Url Check-in Server</Text>
      <TextInput
      style={Styled.textInput}
      placeholder="Config url http://"
      onChangeText={(text) => setUrl(text)}
      onSubmitEditing={handleConfig}
      value={url}
    />
      <View style={Styled.buttonContainer}>
        <TouchableOpacity
          onPress={handleConfig}
          style={[Styled.myButton, { backgroundColor: '#0D2076'}]}
        >
          <Text style={Styled.myText}>Save</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => setVisible(false)}
          style={[Styled.myButton, { backgroundColor: '#9A2320'}]}
        >
          <Text style={Styled.myText}>Cancel</Text>
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={handleResetDefault}
        style={Styled.myButtonReset}
      >
        <Image 
          source={require('../../assets/reset.png')}
          resizeMode='cover'
        />
      </TouchableOpacity>
    </View>
  )
};

ConfigUrlPopUp.propTypes = {
  setVisible: PropTypes.func.isRequired,
}

export default ConfigUrlPopUp;