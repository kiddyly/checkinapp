import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import Styled from '../styled/AutoCheckinPopUpStyled';

const AutoCheckInPopUp = (props) => {
	const {
		popUpData,
		popUpCase,
		closeDialog,
	} = props;

	const [context, setContext] = useState({});

	useEffect(() => {
		let checkInTime = '';
		if (popUpData.statusCode === 200) {
			checkInTime = moment.utc(popUpData.data.checkedInDateTime).format('h:mm a, DD-MM-YYYY');
			setContext({
				fullName: popUpData.data.fullName,
				email: popUpData.data.email,
				checkInTime: checkInTime,
			});
		} else {
			setContext({
				message: popUpData.message,
			});
		}
	})

	function generatePopUp() {
		switch (popUpCase) {
			case 'noExist':
				return (
					<Text style={Styled.myText}>{context.message}</Text>
				);
			case 'checkedIn':
				return (
					<View>
						<Text style={[Styled.myText, {color: 'red'}]}>Guest already checked-in</Text>
						<Text style={Styled.myText}>{context.fullName}</Text>
						<Text style={Styled.myText}>{context.email}</Text>
						<Text style={Styled.itemTextCheckin}>Checked in time: {context.checkInTime}</Text>
					</View>
				);
			case 'Success':
				return (
					<View>
						<Text style={[Styled.myText, {color: 'green'}]}>Check-in Success</Text>
						<Text style={Styled.myText}>{context.fullName}</Text>
						<Text style={Styled.myText}>{context.email}</Text>
						<Text style={Styled.itemTextCheckin}>Checked in time: {context.checkInTime}</Text>
					</View>
				);
		}
	}

	return (
		<View style={Styled.container}>
			{generatePopUp()}
			<TouchableOpacity
				onPress={closeDialog}
				style={Styled.myButtonClose}
			>
				<Text style={[Styled.myText, {color: 'white'}]}>Close</Text>
			</TouchableOpacity>
		</View>
	)
}

AutoCheckInPopUp.propTypes = {
	popUpData: PropTypes.oneOfType([PropTypes.number, PropTypes.object]).isRequired,
	closeDialog: PropTypes.func.isRequired,
	popUpCase: PropTypes.string.isRequired,
}

export default AutoCheckInPopUp;