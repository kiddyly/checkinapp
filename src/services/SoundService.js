import { Audio } from 'expo-av';

export async function playSuccessSound() {
  const soundObject = new Audio.Sound();
  try {
    await soundObject.loadAsync(require('../../assets/sound/goodBeep.mp3'));
    await soundObject.playAsync();
  } catch (error) {
    console.log(error);
  }
};

export async function playFailSound() {
  const soundObject = new Audio.Sound();
  try {
    await soundObject.loadAsync(require('../../assets/sound/badBeep.mp3'));
    await soundObject.playAsync();
  } catch (error) {
    console.log(error);
  }
};