let apiUrl = 'http://checkinevent.azurewebsites.net';

export const configUrl = (url) => {
	apiUrl = url;
	return true;
};

export const getAllGuest = async () => {
	const get = await fetch(`${apiUrl}/api/Guest/CheckedIn`, {
		method: 'GET',
	});
	const response = await get.json();
	return response;
}

export const CheckInGuest = async (activationCode) => {
	const put = await fetch(`${apiUrl}/api/Guest/${activationCode}/CheckedIn`, {
		method: 'PUT',
	});
	const response = await put.json();
	return response;
}