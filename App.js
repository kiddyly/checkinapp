import React, { useState } from 'react';
import Dialog, { DialogContent } from 'react-native-popup-dialog';
import {
          TouchableOpacity,
          Image,
          Text,
          TextInput,
        } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

import QRScannerComponent from './src/component/QRCodeComponent';
import ManualCheckIn from './src/component/ManualCheckInComponent';
import ConfigUrlPopUp from './src/container/ConfigUrlPopUp';
import Styled from './src/styled/AppStyle';

const App = () => {

  // Disable warning key of flatlist
  console.disableYellowBox = true;
  
  const [switchScreen, setSwitchScreen] = useState('autoCheckIn');
  const [visible, setVisible] = useState(false);

  const handleSwitchScreen = () => {
    switch (switchScreen) {
      case 'manualCheckIn':
        setSwitchScreen('autoCheckIn');
        break;
      default:
        setSwitchScreen('manualCheckIn');
        break;
    }
  }

  function generatorScreen() {
    switch (switchScreen) {
      case 'manualCheckIn':
        return (
          <ManualCheckIn />
        )
      default:
        return (
          <QRScannerComponent />
        )
    }
  }

  return (
    <LinearGradient
      style={Styled.container}
      colors={['#FFFFFF', '#0FA1DA', '#0D2076']}
    >
      {generatorScreen()}
      <TouchableOpacity style={Styled.myButton} onPress={handleSwitchScreen}>
        <Image
          style={Styled.icon}
          source={require('./assets/change.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <TouchableOpacity
        style={Styled.myButtonConfig}
        onPress={() => setVisible(true)}
      >
        <Image
          style={Styled.icon}
          source={require('./assets/nut-icon.png')}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <Dialog
        visible={visible}
        onTouchOutside={() => setVisible(false)}
      >
        <DialogContent style={Styled.dialogContent}>
          <ConfigUrlPopUp setVisible={setVisible} />
        </DialogContent>
      </Dialog>
    </LinearGradient>
  );
};

export default App;